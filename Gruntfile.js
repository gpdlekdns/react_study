module.exports = function(grunt) {
    grunt.config.init({
        react: {
            dynamic_mappings: {
                files: [
                    {
                        expand: true,
                        cwd: 'assets/jsx',
                        src: ['**/*.jsx'],
                        dest: 'assets/js',
                        ext: '.js'
                    }
                ]
            }
        },
        watch: {
            files: ['assets/jsx/*.jsx'],
            tasks: ['react']
        }
    })
    grunt.loadNpmTasks('grunt-react');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch'])
};
